output "id" {
  description = "The instance ID."
  value       = join("", aws_instance.this.*.id)
}

output "availability_zone" {
  description = "The availability zone of the instance."
  value       = join("", aws_instance.this.*.availability_zone)
}

output "key_name" {
  description = "The key name of the instance."
  value       = join("", aws_instance.this.*.key_name)
}

output "public_ip" {
  description = "The public IP address assigned to the instance, if applicable."
  # NOTE: If you are using an `aws_eip` with your instance, you should refer to the EIP's address directly and not
  # use `public_ip`, as this field will change after the EIP is attached.
  value = var.ec2_instance_assign_static_eip_address ? join("", aws_eip.ec2_instance.*.public_ip) : join("", aws_instance.this.*.public_ip)
}

output "primary_network_interface_id" {
  description = "ID of the instance's primary network interface"
  value       = join("", aws_instance.this.*.primary_network_interface_id)
}

output "private_ip" {
  description = "The private IP address assigned to the instance."
  value       = join("", aws_instance.this.*.private_ip)
}

output "security_groups" {
  description = "The associated security groups."
  value       = aws_instance.this.*.vpc_security_group_ids
}

output "subnet_id" {
  description = "The VPC subnet ID."
  value       = join("", aws_instance.this.*.subnet_id)
}

output "role" {
  description = "Name of AWS IAM Role associated with the instance"
  value       = join("", aws_iam_role.ec2_instance.*.name)
}

output "alarm" {
  description = "CloudWatch Alarm ID"
  value       = join("", aws_cloudwatch_metric_alarm.ec2_instance_reboot.*.id)
}

output "ebs_ids" {
  description = "IDs of EBSs"
  value       = compact(coalescelist(aws_ebs_volume.this.*.id, [""]))
}
