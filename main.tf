locals {
  enabled = var.ec2_instance_module_enabled ? true : false

  tags = merge(
    var.ec2_instance_tags,
    {
      terraform = true
    }
  )
}

data "aws_subnet" "this" {
  # Provides details about a specific VPC subnet.
  # This resource can prove useful when a module accepts a subnet id as an input variable and needs to, for example,
  # determine the id of the VPC that the subnet belongs to.
  count = local.enabled ? 1 : 0

  # (Optional) The id of the specific subnet to retrieve.
  id = var.ec2_instance_subnet_id
}
locals {
  availability_zone = length(var.ec2_instance_availability_zone) > 0 ? var.ec2_instance_availability_zone : join("", data.aws_subnet.this.*.availability_zone)
}

# ---------------------------
# Network/Security resources
# ---------------------------
resource "aws_security_group" "ec2" {
  # Provides a security group resource.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("%s-ec2-sg-", var.ec2_instance_name)
  # (Optional, Forces new resource) The security group description. Defaults to "Managed by Terraform".
  description = "Security Group for the [${var.ec2_instance_name}] EC2 instance."
  # (Optional, Forces new resource) The VPC ID.
  vpc_id = var.ec2_instance_vpc_id
  # (Optional, Forces new resource) The security group description. Defaults to "Managed by Terraform".
  tags = merge(
    local.tags,
    {
      Name = format("%s-ec2-instance-sg", var.ec2_instance_name)
    }
  )

  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_security_group_rule" "ec2_allow_all_egress" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = local.enabled ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "egress"

  # (Optional) Description of the rule.
  description = "Allow ALL Egress traffic from the [${var.ec2_instance_name}] EC2 instance."

  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 0
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = ["0.0.0.0/0"]

  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.ec2.*.id)
}
resource "aws_security_group_rule" "ec2_allow_ingress_ports" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = local.enabled ? length(compact(var.ec2_instance_allowed_ingress_ports)) : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"

  # (Optional) Description of the rule.
  description = "Allow ingress traffic to the [${var.ec2_instance_name}] EC2 instance on the [${var.ec2_instance_allowed_ingress_ports[count.index]}] port from anywhere."

  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = var.ec2_instance_allowed_ingress_ports[count.index]
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = var.ec2_instance_allowed_ingress_ports[count.index]
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "tcp"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = ["0.0.0.0/0"]

  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.ec2.*.id)
}
resource "aws_security_group_rule" "ec2_allow_ingress_cidrs" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = (local.enabled && length(compact(var.ec2_instance_allowed_ingress_cidrs)) > 0) ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"

  # (Optional) Description of the rule.
  description = "Allow ALL the ingress traffic to the [${var.ec2_instance_name}] EC2 instance from the specified CIDR blocks."

  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 65535
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = var.ec2_instance_allowed_ingress_cidrs

  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.ec2.*.id)
}
resource "aws_security_group_rule" "ec2_allow_ingress_security_groups" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = local.enabled ? length(compact(var.ec2_instance_allowed_security_groups)) : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"

  # (Optional) Description of the rule.
  description = "Allow ALL ingress traffic to the [${var.ec2_instance_name}] EC2 instance from the [${var.ec2_instance_allowed_security_groups[count.index]}] Security Group."

  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 0
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"
  # (Optional) The security group id to allow access to/from, depending on the type. Cannot be specified with
  # `cidr_blocks` and `self`.
  source_security_group_id = var.ec2_instance_allowed_security_groups[count.index]

  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.ec2.*.id)
}

data "aws_iam_policy_document" "ec2_assume_role" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the aws_iam_policy resource.
  count = local.enabled ? 1 : 0

  statement {
    sid    = "AllowEC2InstanceToAssumeThisRole"
    effect = "Allow"
    actions = [
      "sts:AssumeRole"
    ]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}
resource "aws_iam_role" "ec2_instance" {
  # Provides an IAM role.

  # NOTE: If policies are attached to the role via the `aws_iam_policy_attachment` resource and you are modifying the
  # role `name` or `path`, the `force_detach_policies` argument must be set to `true` and applied before attempting the
  # operation otherwise you will encounter a `DeleteConflict` error.
  # The `aws_iam_role_policy_attachment` resource (recommended) does not have this requirement.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("%s-ec2-role-", var.ec2_instance_name)
  # (Optional) The description of the role.
  description = "The IAM role for the [${var.ec2_instance_name}] EC2 instance."
  # Key-value mapping of tags for the IAM role.
  tags = merge(
    local.tags,
    {
      Name = format("%s-ec2-role", var.ec2_instance_name)
    }
  )

  # (Optional) The path to the role. See IAM Identifiers for more information:
  #   * https://docs.aws.amazon.com/IAM/latest/UserGuide/Using_Identifiers.html
  path = "/"
  # (Required) The policy that grants an entity permission to assume the role.
  assume_role_policy = join("", data.aws_iam_policy_document.ec2_assume_role.*.json)
}
resource "aws_iam_role_policy_attachment" "cw_agent" {
  # Attaches a Managed IAM Policy to an IAM role.
  # NOTE(!): the usage of this resource conflicts with the aws_iam_policy_attachment resource and will permanently
  # show a difference if both are defined.
  count = local.enabled ? 1 : 0
  # (Required) The role the policy should be applied to.
  role = join("", aws_iam_role.ec2_instance.*.name)
  # (Required) The ARN of the policy you want to apply.
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}
resource "aws_iam_role_policy_attachment" "cw_logs_ro" {
  # Attaches a Managed IAM Policy to an IAM role.
  # NOTE(!): the usage of this resource conflicts with the aws_iam_policy_attachment resource and will permanently
  # show a difference if both are defined.
  count = local.enabled ? 1 : 0
  # (Required) The role the policy should be applied to.
  role = join("", aws_iam_role.ec2_instance.*.name)
  # (Required) The ARN of the policy you want to apply.
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsReadOnlyAccess"
}
resource "aws_iam_role_policy_attachment" "ssm_core" {
  # Attaches a Managed IAM Policy to an IAM role.
  # NOTE(!): the usage of this resource conflicts with the aws_iam_policy_attachment resource and will permanently
  # show a difference if both are defined.
  count = local.enabled ? 1 : 0
  # (Required) The role the policy should be applied to.
  role = join("", aws_iam_role.ec2_instance.*.name)
  # (Required) The ARN of the policy you want to apply.
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

data "aws_iam_policy_document" "ec2_minimal" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the aws_iam_policy resource.
  count = local.enabled ? 1 : 0

  statement {
    sid = "GiveEC2InstancesMorePermissions"
    actions = [
      "ec2:Describe*",
      "ec2:List*"
    ]
    effect    = "Allow"
    resources = ["*"]
  }
}
data "aws_iam_policy_document" "empty" {
  count = (local.enabled && var.ec2_instance_iam_policy == "") ? 1 : 0
}
data "aws_iam_policy_document" "ec2_instance" {
  count = local.enabled ? 1 : 0

  # (Optional) - An IAM policy document to import as a base for the current policy document. Statements with
  # non-blank sids in the current policy document will overwrite statements with the same sid in the source json.
  # Statements without an sid cannot be overwritten.
  source_json = var.ec2_instance_iam_policy == "" ? join("", data.aws_iam_policy_document.empty.*.json) : var.ec2_instance_iam_policy
  # (Optional) - An IAM policy document to import and override the current policy document. Statements with non-blank
  # sids in the override document will overwrite statements with the same sid in the current document. Statements
  # without an sid cannot be overwritten.
  override_json = join("", data.aws_iam_policy_document.ec2_minimal.*.json)
}
resource "aws_iam_role_policy" "ec2_instance" {
  # Provides an IAM role policy.
  count = local.enabled ? 1 : 0

  # (Optional) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("%s-ec2-policy-", var.ec2_instance_name)
  # (Required) The policy document. This is a JSON formatted string.
  policy = join("", data.aws_iam_policy_document.ec2_instance.*.json)
  # (Required) The IAM role to attach to the policy.
  role = join("", aws_iam_role.ec2_instance.*.id)
}

resource "aws_iam_instance_profile" "this" {
  # Provides an IAM instance profile.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with name.
  name_prefix = format("%s-instance-profile-", var.ec2_instance_name)
  # (Optional) The role name to include in the profile.
  role = join("", aws_iam_role.ec2_instance.*.name)
}


# -----------------------
# EC2 instance resources
# -----------------------
resource "tls_private_key" "ec2" {
  # Generates a secure private key and encodes it as PEM. This resource is primarily intended for easily bootstrapping
  # throwaway development environments.
  #
  # Important Security Notice!
  # The private key generated by this resource will be stored unencrypted in your Terraform state file. Use of this
  # resource for production deployments is not recommended. Instead, generate a private key file outside of Terraform
  # and distribute it securely to the system where Terraform will be run.
  #
  # This is a logical resource, so it contributes only to the current Terraform state and does not create any external
  # managed resources.
  count = local.enabled ? 1 : 0

  # (Required) The name of the algorithm to use for the key. Currently-supported values are "RSA" and "ECDSA".
  algorithm = "RSA"
}
resource "aws_key_pair" "ec2" {
  # Provides an EC2 key pair resource. A key pair is used to control login access to EC2 instances.
  # Currently this resource requires an existing user-supplied key pair. This key pair's public key will be registered
  # with AWS to allow logging-in to EC2 instances.
  # When importing an existing key pair the public key material may be in any format supported by AWS. Supported formats
  # (per the AWS documentation) are:
  #   * OpenSSH public key format (the format in ~/.ssh/authorized_keys)
  #   * Base64 encoded DER format
  #   * SSH public key file format as specified in RFC4716
  count = local.enabled ? 1 : 0

  # (Optional) Creates a unique name beginning with the specified prefix. Conflicts with key_name.
  key_name_prefix = format("%s-ssh-key-", var.ec2_instance_name)
  # (Required) The public key material.
  public_key = length(var.ec2_instance_ssh_public_key) > 0 ? var.ec2_instance_ssh_public_key : join("", tls_private_key.ec2.*.public_key_openssh)
}

data "aws_ami" "this" {
  # Use this data source to get the ID of a registered AMI for use in other resources.
  count = local.enabled && length(var.ec2_instance_ami_id) == 0 ? 1 : 0

  # (Required) List of AMI owners to limit search. At least 1 value must be specified. Valid values:
  #   * an AWS account ID
  #   * self (the current account)
  #   * an AWS owner alias (e.g. amazon, aws-marketplace, microsoft).
  owners = [
    var.ec2_instance_ami_owner
  ]

  # (Optional) If more than one result is returned, use the most recent AMI.
  most_recent = "true"

  # (Optional) One or more name/value pairs to filter off of. There are several valid keys, for a full reference,
  # check out describe-images in the AWS CLI reference:
  #   * http://docs.aws.amazon.com/cli/latest/reference/ec2/describe-images.html
  filter {
    name = "name"
    values = [
      var.ec2_instance_ami_name
    ]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "this" {
  # Provides an EC2 instance resource. This allows instances to be created, updated, and deleted. Instances also support
  # provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.enabled ? 1 : 0

  # (Required) The AMI to use for the instance.
  ami = length(var.ec2_instance_ami_id) > 0 ? var.ec2_instance_ami_id : join("", data.aws_ami.this.*.id)
  # (Required) The type of instance to start. Updates to this field will trigger a stop/start of the EC2 instance.
  instance_type = var.ec2_instance_type

  # (Optional) The AZ to start the instance in.
  availability_zone = local.availability_zone
  # (Optional) If true, the launched EC2 instance will be EBS-optimized.
  # Note(!): that if this is not set on an instance type that is optimized by default then this will show as disabled
  # but if the instance type is optimized by default then there is no need to set this and there is no effect
  # to disabling it.
  ebs_optimized = var.ec2_instance_ebs_optimized
  # (Optional) If true, enables EC2 Instance Termination Protection
  disable_api_termination = var.ec2_instance_disable_api_termination
  # (Optional) The key name of the Key Pair to use for the instance; which can be managed using the `aws_key_pair`
  # resource.
  key_name = var.ec2_instance_ssh_key_name != "" ? var.ec2_instance_ssh_key_name : join("", aws_key_pair.ec2.*.key_name)
  # (Optional) If true, the launched EC2 instance will have detailed monitoring enabled.
  monitoring = var.ec2_instance_detailed_monitoring_enabled
  # (Optional, VPC only) A list of security group IDs to associate with.
  vpc_security_group_ids = compact(concat(var.ec2_instance_security_groups, coalescelist([join("", aws_security_group.ec2.*.id)], [""])))
  # (Optional) The VPC Subnet ID to launch in.
  subnet_id = var.ec2_instance_subnet_id
  # (Optional) Associate a public ip address with an instance in a VPC. Boolean value.
  associate_public_ip_address = var.ec2_instance_associate_public_ip_address
  # (Optional) Private IP address to associate with the instance in a VPC.
  private_ip = var.ec2_instance_private_ip
  # (Optional) Controls if traffic is routed to the instance when the destination address does not match the instance.
  # Used for NAT or VPNs. Defaults true.
  source_dest_check = var.ec2_instance_source_dest_check
  # (Optional) Can be used instead of user_data to pass base64-encoded binary data directly. Use this instead of
  # `user_data` whenever the value is not a valid UTF-8 string. For example, gzip-encoded user data must be
  # base64-encoded and passed via this argument to avoid corruption.
  user_data_base64 = var.ec2_instance_user_data_base64
  # (Optional) The IAM Instance Profile to launch the instance with. Specified as the name of the Instance Profile.
  # Ensure your credentials have the correct permission to assign the instance profile according to the
  # EC2 documentation, notably `iam:PassRole`.
  iam_instance_profile = join("", aws_iam_instance_profile.this.*.name)
  # (Optional) A number of IPv6 addresses to associate with the primary network interface. Amazon EC2 chooses the
  # IPv6 addresses from the range of your subnet.
  ipv6_address_count = var.ec2_instance_ipv6_address_count
  # (Optional) Specify one or more IPv6 addresses from the range of the subnet to associate with the primary network
  # interface.
  ipv6_addresses = var.ec2_instance_ipv6_addresses

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = var.ec2_instance_name
    }
  )

  # Customize details about the root block device of the instance.
  # Each of the `*_block_device` attributes control a portion of the AWS Instance's "Block Device Mapping". See
  # AWS's Block Device Mapping docs to understand the implications of using these attributes:
  #   * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/block-device-mapping-concepts.html
  root_block_device {
    # (Optional) The type of volume. Can be:
    #   * standard
    #   * gp2
    #   * io1
    #   * sc1
    #   * st1
    volume_type = var.ec2_instance_root_volume_type
    # (Optional) The amount of provisioned IOPS. This is only valid for volume_type of "io1", and must be specified
    # if using that type
    iops = lower(var.ec2_instance_root_volume_type) == "io1" ? var.ec2_instance_root_volume_iops : 0
    # (Optional) The size of the volume in gibibytes (GiB).
    volume_size = var.ec2_instance_root_volume_size
    # (Optional) Whether the volume should be destroyed on instance termination (Default: true).
    delete_on_termination = var.ec2_instance_root_volume_delete_on_termination
  }
  # NOTE: Currently, changes to the `ebs_block_device` configuration of existing resources cannot be automatically
  # detected by Terraform. To manage changes and attachments of an EBS block to an instance, use the `aws_ebs_volume`
  # and `aws_volume_attachment` resources instead. If you use `ebs_block_device` on an `aws_instance`, Terraform will
  # assume management over the full set of non-root EBS block devices for the instance, treating additional block
  # devices as drift!! For this reason, `ebs_block_device` cannot be mixed with external `aws_ebs_volume` and
  # `aws_volume_attachment` resources for a given instance.
}

### Additional EBS volumes ###
resource "aws_ebs_volume" "this" {
  # Manages a single EBS volume.
  count = local.enabled ? length(var.ec2_instance_ebs_volumes_list) : 0

  # (Required) The AZ where the EBS volume will exist.
  availability_zone = local.availability_zone

  # (Optional) The type of EBS volume. Can be "standard", "gp2", "io1", "sc1" or "st1" (Default: "standard").
  type = lookup(var.ec2_instance_ebs_volumes_list[count.index], "volume_type", "standard")
  # (Optional) The amount of IOPS to provision for the disk.
  iops = lookup(var.ec2_instance_ebs_volumes_list[count.index], "volume_iops", 0)

  # NOTE: One of size or snapshot_id is required when specifying an EBS volume

  # (Optional) The size of the drive in GiBs.
  size = lookup(var.ec2_instance_ebs_volumes_list[count.index], "volume_size", null)

  tags = merge(
    local.tags,
    {
      Name = format(
        "%s-ebs-%s-volume-%s",
        var.ec2_instance_name,
        lookup(var.ec2_instance_ebs_volumes_list[count.index], "volume_type", "standard"),
        count.index
      )
    }
  )
}
locals {
  # Name of the EBS device to mount
  device_names_list = [
    "/dev/xvdb", "/dev/xvdc", "/dev/xvdd", "/dev/xvde", "/dev/xvdf", "/dev/xvdg", "/dev/xvdh", "/dev/xvdi",
    "/dev/xvdj", "/dev/xvdk", "/dev/xvdl", "/dev/xvdm", "/dev/xvdn", "/dev/xvdo", "/dev/xvdp", "/dev/xvdq",
    "/dev/xvdr", "/dev/xvds", "/dev/xvdt", "/dev/xvdu", "/dev/xvdv", "/dev/xvdw", "/dev/xvdx", "/dev/xvdy",
    "/dev/xvdz"
  ]
}
resource "aws_volume_attachment" "default" {
  # Provides an AWS EBS Volume Attachment as a top level resource, to attach and detach volumes from AWS Instances.
  count = local.enabled ? length(var.ec2_instance_ebs_volumes_list) : 0

  # (Required) ID of the Volume to be attached
  volume_id = aws_ebs_volume.this.*.id[count.index]

  # (Required) The device name to expose to the instance (for example, /dev/sdh or xvdh). See Device Naming on Linux
  # Instances:
  #   * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/device_naming.html#available-ec2-device-names
  # and Device Naming on Windows Instances for more information:
  #   * https://docs.aws.amazon.com/AWSEC2/latest/WindowsGuide/device_naming.html#available-ec2-device-names
  device_name = local.device_names_list[count.index]
  # (Required) ID of the Instance to attach to.
  instance_id = join("", aws_instance.this.*.id)
}

resource "aws_eip" "ec2_instance" {
  # Provides an Elastic IP resource.

  # Note(1): EIP may require IGW to exist prior to association. Use depends_on to set an explicit dependency on the IGW.

  # Note(2): Do not use `network_interface` to associate the EIP to `aws_lb` or `aws_nat_gateway` resources. Instead use
  # the `allocation_id` available in those resources to allow AWS to manage the association, otherwise you will see
  # `AuthFailure` errors.
  count = local.enabled && var.ec2_instance_associate_public_ip_address && var.ec2_instance_assign_static_eip_address ? 1 : 0

  # See:
  #   * https://discuss.hashicorp.com/t/tips-howto-implement-module-depends-on-emulation/2305
  depends_on = [
    var.ec2_instance_module_depends_on
  ]

  # (Optional) Boolean if the EIP is in a VPC or not.
  vpc = true
  # (Optional) Network interface ID to associate with.
  network_interface = join("", aws_instance.this.*.primary_network_interface_id)

  # (Optional) A mapping of tags to assign to the resource.
  # Override the "Name" tag from `local.vpc_tags`.
  tags = merge(
    local.tags,
    { Name = format("%s-ec2-instance-eip", var.ec2_instance_name) },
  )

  lifecycle {
    create_before_destroy = true
  }
}


# --------------------------------------------------
# CloudWatch Alarm to restart dead or hung instance
# --------------------------------------------------
data "aws_caller_identity" "current" {
  # Use this data source to get the access to the effective Account ID, User ID, and ARN in which Terraform is
  # authorized.
  count = (local.enabled && var.ec2_instance_detailed_monitoring_enabled) ? 1 : 0
}

data "aws_region" "current" {
  # Provides details about a specific AWS region.
  # As well as validating a given region name this resource can be used to discover the name of the region configured
  # within the provider. The latter can be useful in a child module which is inheriting an AWS provider configuration
  # from its parent module.
  count = local.enabled ? 1 : 0
}

resource "null_resource" "check_alarm_action" {
  # The null_resource resource implements the standard resource lifecycle but takes no further action.
  count = (local.enabled && var.ec2_instance_detailed_monitoring_enabled) ? 1 : 0

  # The triggers argument allows specifying an arbitrary set of values that, when changed, will cause the resource
  # to be replaced.
  triggers = {
    action = "arn:aws:swf:${join("", data.aws_region.current.*.name)}:${join("", data.aws_caller_identity.current.*.account_id)}:${var.ec2_instance_cloudwatch_alarm_default_action}"
  }
}

resource "random_uuid" "alarm" {
  # The resource random_uuid generates random uuid string that is intended to be used as unique identifiers for other
  # resources.
  # This resource uses the hashicorp/go-uuid to generate a UUID-formatted string for use with services needed a unique
  # string identifier.
  count = (local.enabled && var.ec2_instance_detailed_monitoring_enabled) ? 1 : 0

  # (Optional) Arbitrary map of values that, when changed, will trigger a new uuid to be generated.
  keepers = {
    # Generate a new ID each time we changed a name.
    ec2_instance_name = var.ec2_instance_name
  }
}

resource "aws_cloudwatch_metric_alarm" "ec2_instance_reboot" {
  # Provides a CloudWatch Metric Alarm resource.
  count = (local.enabled && var.ec2_instance_detailed_monitoring_enabled) ? 1 : 0

  depends_on = [
    null_resource.check_alarm_action
  ]

  # (Required) The descriptive name for the alarm. This name must be unique within the user's AWS account
  alarm_name = format("%s-cloudwatch-alarm-%s", var.ec2_instance_name, join("", random_uuid.alarm.*.result))
  # (Required) The arithmetic operation to use when comparing the specified Statistic and Threshold. The specified
  # Statistic value is used as the first operand. Either of the following is supported:
  #   * GreaterThanOrEqualToThreshold
  #   * GreaterThanThreshold
  #   * LessThanThreshold
  #   * LessThanOrEqualToThreshold.
  comparison_operator = var.ec2_instance_cloudwatch_comparison_operator
  # (Required) The number of periods over which data is compared to the specified threshold.
  evaluation_periods = var.ec2_instance_cloudwatch_evaluation_periods
  # (Optional) The name for the alarm's associated metric. See docs for supported metrics:
  #   * https://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/CW_Support_For_AWS.html
  metric_name = var.ec2_instance_cloudwatch_metric_name
  # (Optional) The namespace for the alarm's associated metric. See docs for the list of namespaces:
  #   * https://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/aws-namespaces.html
  namespace = var.ec2_instance_cloudwatch_metric_namespace
  # (Optional) The statistic to apply to the alarm's associated metric. Either of the following is supported:
  #   * SampleCount
  #   * Average
  #   * Sum
  #   * Minimum
  #   * Maximum
  statistic = var.ec2_instance_cloudwatch_statistic
  # (Optional) The period in seconds over which the specified `statistic` is applied.
  period = var.ec2_instance_cloudwatch_applying_period
  # (Required) The value against which the specified statistic is compared.
  threshold = var.ec2_instance_cloudwatch_metric_threshold
  # (Optional) The dimensions for the alarm's associated metric. For the list of available dimensions see the AWS
  # documentation:
  #   * http://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/CW_Support_For_AWS.html
  dimensions = {
    InstanceId = join("", aws_instance.this.*.id)
  }
  # (Optional) The list of actions to execute when this alarm transitions into an ALARM state from any other state.
  # Each action is specified as an Amazon Resource Name (ARN).
  alarm_actions = [
    join("", null_resource.check_alarm_action.*.triggers.action)
  ]
}
