# ------------------------
# Common/General settings
# ------------------------
variable "ec2_instance_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default = false
}
variable "ec2_instance_module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default = null
}

variable "ec2_instance_name" {
  description = "(Required) A name associated with the EC2 instance to distinct from others."
  type        = string
}
variable "ec2_instance_tags" {
  description = "Additional mapping of tags to assign to the all linked resources."
  type        = map(string)
  default     = {}
}


# ----------------------
# EC2 instance settings
# ----------------------
### Global ###
variable "ec2_instance_availability_zone" {
  description = "Availability Zone to launch the instance in. If not set, will be launched in the first AZ of the region."
  type        = string
  default     = ""
}
### Network/Security ###
variable "ec2_instance_subnet_id" {
  description = "(Required) VPC Subnet ID to launch the instance in."
  type        = string
}
variable "ec2_instance_vpc_id" {
  description = "(Required) The ID of the VPC that the instance to launch the instance in."
  type        = string
}
variable "ec2_instance_security_groups" {
  description = "(Optional, VPC only) A list of security group IDs to associate with."
  type        = list(string)
  default     = []
}

variable "ec2_instance_allowed_ingress_ports" {
  description = "List of allowed ingress ports."
  type        = list(number)
  default     = []
}
variable "ec2_instance_allowed_ingress_cidrs" {
  description = "List of CIDR blocks ALL the ingress traffic allowed from."
  type        = list(string)
  default     = []
}
variable "ec2_instance_allowed_security_groups" {
  description = "List of Security Group IDs allowed to connect to the instance."
  type        = list(string)
  default     = []
}
variable "ec2_instance_iam_policy" {
  description = "The IAM policy as JSON formatted string to include into instance profile."
  type        = string
  default     = ""
}

### EC2 specific ###
variable "ec2_instance_ami_id" {
  description = "The AMI to use for the instance."
  type        = string
  default     = ""
}
variable "ec2_instance_ami_name" {
  description = "The name filter to find the most recent AMI."
  type        = string
  default     = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"
}
variable "ec2_instance_ami_owner" {
  description = "Owner of the given AMI."
  type        = string
  # Canonical
  default = "099720109477"
}
variable "ec2_instance_type" {
  description = "The type of the instance."
  type        = string
  default     = "t3a.micro"
}
variable "ec2_instance_ebs_optimized" {
  description = "Launched EC2 instance will be EBS-optimized."
  type        = bool
  default     = false
}
variable "ec2_instance_disable_api_termination" {
  description = "Enable EC2 Instance Termination Protection."
  type        = bool
  default     = false
}

# NOTE(!): one of the SSH_* variables must be non empty
variable "ec2_instance_ssh_key_name" {
  description = "The name of the SSH key pair to be provisioned on the instance."
  # If empty a new key pair will be created using `ec2_instance_ssh_public_key` value.
  type    = string
  default = ""
}
variable "ec2_instance_ssh_public_key" {
  description = "The public key material to create a new SSH key pair resource."
  type        = string
  default     = ""
}

variable "ec2_instance_detailed_monitoring_enabled" {
  description = "Launched EC2 instance will have detailed monitoring enabled."
  type        = bool
  default     = false
}
variable "ec2_instance_associate_public_ip_address" {
  description = "Associate a public IP address with the instance."
  type        = bool
  default     = true
}
variable "ec2_instance_private_ip" {
  description = "Private IP address to associate with the instance in the VPC."
  type        = string
  default     = ""
}
variable "ec2_instance_source_dest_check" {
  description = "Controls if traffic is routed to the instance when the destination address does not match the instance. Used for NAT or VPNs."
  type        = bool
  default     = true
}
variable "ec2_instance_user_data_base64" {
  description = "Can be used instead of `user_data` to pass base64-encoded binary data directly, e.g. instance bootstrap script base64-encoded."
  type        = string
  default     = ""
}
variable "ec2_instance_ipv6_address_count" {
  description = "Number of IPv6 addresses to associate with the primary network interface. Amazon EC2 chooses the IPv6 addresses from the range of your subnet."
  type        = number
  default     = 0
}
variable "ec2_instance_ipv6_addresses" {
  description = "List of IPv6 addresses from the range of the subnet to associate with the primary network interface."
  type        = list(string)
  default     = []
}
### Root volume ###
variable "ec2_instance_root_volume_type" {
  description = "Type of root volume."
  type        = string
  default     = "gp2"
}
variable "ec2_instance_root_volume_iops" {
  description = "Amount of provisioned IOPS. This must be set if `root_volume_type` is set or detected to `io1` value."
  type        = number
  default     = 0
}
variable "ec2_instance_root_volume_size" {
  description = "Size of the root volume in gigabytes."
  type        = number
  default     = 10
}
variable "ec2_instance_root_volume_delete_on_termination" {
  description = "Whether the volume should be destroyed on instance termination."
  type        = bool
  default     = true
}
### additional EBS volumes ###
variable "ec2_instance_ebs_volumes_list" {
  description = "The list of map describing single EBS volume."
  type = list(object({
    # The type of EBS volume. Can be "standard", "gp2", "io1", "sc1" or "st1"
    volume_type = string
    # The amount of IOPS to provision for the disk.
    volume_iops = number
    # The size of the drive in GiBs.
    volume_size = number
  }))
  default = []
}

### Static EIP address ###
variable "ec2_instance_assign_static_eip_address" {
  description = "Assign an Elastic IP address to the instance."
  type        = bool
  default     = false
}


# ----------------------------------
# CloudWatch Alarm for the instance
# ----------------------------------
variable "ec2_instance_cloudwatch_alarm_default_action" {
  description = "Default alarm action."
  type        = string
  default     = "action/actions/AWS_EC2.InstanceId.Reboot/1.0"
}
variable "ec2_instance_cloudwatch_comparison_operator" {
  description = "The arithmetic operation to use when comparing the specified Statistic and Threshold."
  # Either of the following is supported:
  #   * GreaterThanOrEqualToThreshold
  #   * GreaterThanThreshold
  #   * LessThanThreshold
  #   * LessThanOrEqualToThreshold.
  type    = string
  default = "GreaterThanOrEqualToThreshold"
}
variable "ec2_instance_cloudwatch_evaluation_periods" {
  description = "The number of periods over which data is compared to the specified threshold."
  type        = number
  default     = 5
}
variable "ec2_instance_cloudwatch_metric_name" {
  description = "The name for the alarm's associated metric."
  # Allowed values can be found in https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/ec2-metricscollected.html
  type    = string
  default = "StatusCheckFailed_Instance"
}
variable "ec2_instance_cloudwatch_metric_namespace" {
  description = "The namespace for the alarm's associated metric."
  # Allowed values can be found in https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/aws-namespaces.html
  type    = string
  default = "AWS/EC2"
}
variable "ec2_instance_cloudwatch_statistic" {
  description = "The statistic to apply to the alarm's associated metric. Allowed values are: SampleCount, Average, Sum, Minimum, Maximum."
  type        = string
  default     = "Maximum"
}
variable "ec2_instance_cloudwatch_applying_period" {
  description = "The period in seconds over which the specified statistic is applied."
  type        = number
  default     = 60
}
variable "ec2_instance_cloudwatch_metric_threshold" {
  description = "The value against which the specified statistic is compared."
  type        = number
  default     = 1
}
