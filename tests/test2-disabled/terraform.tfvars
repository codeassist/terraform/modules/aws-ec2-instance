# ------------------------
# Common/General settings
# ------------------------
# Whether to create the resources (`false` prevents the module from creating any resources).
ec2_instance_module_enabled = false
# Emulation of `depends_on` behavior for the module.
# Non zero length string can be used to have current module wait for the specified resource.
ec2_instance_module_depends_on = ""
# (Required) A name associated with the EC2 instance to distinct from others.
ec2_instance_name = "test2"
# Additional mapping of tags to assign to the all linked resources.
ec2_instance_tags = {}

# ----------------------
# EC2 instance settings
# ----------------------
### Global ###
# Availability Zone to launch the instance in. If not set, will be launched in the first AZ of the region.
ec2_instance_availability_zone = ""
### Network/Security ###
# (Required) VPC Subnet ID to launch the instance in.
ec2_instance_subnet_id = "subnet-065bee2d"
# (Required) The ID of the VPC that the instance to launch the instance in.
ec2_instance_vpc_id = "vpc-76471513"
# (Optional, VPC only) A list of security group IDs to associate with.
ec2_instance_security_groups = []
# List of allowed ingress ports.
ec2_instance_allowed_ingress_ports = []
# List of CIDR blocks ALL the ingress traffic allowed from.
ec2_instance_allowed_ingress_cidrs = []
# List of Security Group IDs allowed to connect to the instance.
ec2_instance_allowed_security_groups = []
# The IAM policy as JSON formatted string to include into instance profile.
ec2_instance_iam_policy = ""

### EC2 specific ###
# The AMI to use for the instance.
ec2_instance_ami_id = ""
# The name filter to find the most recent AMI.
ec2_instance_ami_name = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"
# Owner of the given AMI.
ec2_instance_ami_owner = "099720109477"
# The type of the instance.
ec2_instance_type = "t3a.micro"
# Launched EC2 instance will be EBS-optimized.
ec2_instance_ebs_optimized = false
# Enable EC2 Instance Termination Protection.
ec2_instance_disable_api_termination = false
# Whether to use SSH key pair by specified name or to create another one (useful to avoid `count can't be computed` error).
#ec2_instance_use_ssh_key_by_name = true
# SSH key pair to be provisioned on the instance. If empty a new key pair will be created using `ec2_instance_ssh_public_key` value.
ec2_instance_ssh_key_name = "ssh-key-pair-test1"
# The public key material to create a SSH key pair resource.
ec2_instance_ssh_public_key = ""
# Launched EC2 instance will have detailed monitoring enabled.
ec2_instance_detailed_monitoring_enabled = false
# Associate a public IP address with the instance.
ec2_instance_associate_public_ip_address = true
# Private IP address to associate with the instance in the VPC.
ec2_instance_private_ip = ""
# Controls if traffic is routed to the instance when the destination address does not match the instance. Used for NAT or VPNs.
ec2_instance_source_dest_check = true
# Can be used instead of `user_data` to pass base64-encoded binary data directly, e.g. instance bootstrap script base64-encoded.
ec2_instance_user_data_base64 = ""
# Number of IPv6 addresses to associate with the primary network interface. Amazon EC2 chooses the IPv6 addresses from the range of your subnet.
ec2_instance_ipv6_address_count = 0
# List of IPv6 addresses from the range of the subnet to associate with the primary network interface.
ec2_instance_ipv6_addresses = []
### Root volume ###
# Type of root volume.
ec2_instance_root_volume_type = "gp2"
# Amount of provisioned IOPS. This must be set if `root_volume_type` is set or detected to `io1` value.
ec2_instance_root_volume_iops = 0
# Size of the root volume in gigabytes.
ec2_instance_root_volume_size = 10
# Whether the volume should be destroyed on instance termination.
ec2_instance_root_volume_delete_on_termination = true
### additional EBS volumes ###
# The list of map describing single EBS volume.
ec2_instance_ebs_volumes_list = []
### Static EIP address ###
# Assign an Elastic IP address to the instance.
ec2_instance_assign_static_eip_address = false

# ----------------------------------
# CloudWatch Alarm for the instance
# ----------------------------------
# Default alarm action.
ec2_instance_cloudwatch_alarm_default_action = "action/actions/AWS_EC2.InstanceId.Reboot/1.0"
# The arithmetic operation to use when comparing the specified Statistic and Threshold. Either of the following
# is supported:
#   * GreaterThanOrEqualToThreshold
#   * GreaterThanThreshold
#   * LessThanThreshold
#   * LessThanOrEqualToThreshold.
ec2_instance_cloudwatch_comparison_operator = "GreaterThanOrEqualToThreshold"
# The number of periods over which data is compared to the specified threshold.
ec2_instance_cloudwatch_evaluation_periods = 5
# The name for the alarm's associated metric. Allowed values can be found in:
#   * https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/ec2-metricscollected.html
ec2_instance_cloudwatch_metric_name = "StatusCheckFailed_Instance"
# The namespace for the alarm's associated metric. Allowed values can be found in:
#   * https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/aws-namespaces.html
ec2_instance_cloudwatch_metric_namespace = "AWS/EC2"
# The statistic to apply to the alarm's associated metric. Allowed values are: SampleCount, Average, Sum, Minimum, Maximum.
ec2_instance_cloudwatch_statistic = "Maximum"
# The period in seconds over which the specified statistic is applied.
ec2_instance_cloudwatch_applying_period = 60
# The value against which the specified statistic is compared.
ec2_instance_cloudwatch_metric_threshold = 1
